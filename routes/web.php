<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/index','Project1DatabaseController@index');
Route::get('/create','Project1DatabaseController@create');
Route::post('/store','Project1DatabaseController@store');
// tulisan hitam: best URL
// tulisan abu-abu: extention URL