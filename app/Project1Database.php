<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project1Database extends Model
{
    protected $fillable=['computer_type','computer_price'];
}
