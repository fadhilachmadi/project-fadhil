<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Project1Database;

class Project1DatabaseController extends Controller
{
    public function index(){
        $datas=Project1Database::all();
        return view('index',compact('datas'));
    }

    public function create(){
        return view ('create');
    }

    public function store(Request $request){
        // untuk simpan data ke database
        Project1Database::create(
            [
                'computer_type'=>$request['computer_type'],
                'computer_price'=>$request['computer_price']
            ]


        );
        return redirect("/index");
    }
}
